from typing import Union, Optional
from pydantic import BaseModel
from fastapi import FastAPI, status
# from src.data_function import export_data_as_excel
from src.ProductSalesAmount_export import export_data
from src.ProductSalesAmount_ingest import Truncate_raw_ProductSalesAmount, Load_excel_as_dataframe, Load_dataframe_to_raw, Transform_and_Load
from src.ProductSalesAmount_rerun import Truncate_raw_ProductSalesAmount, Load_excel_as_dataframe, Load_dataframe_to_raw, Transform_and_Load, delete_newer_data
from src.notification import sendEmail
app = FastAPI()


class DateQuery(BaseModel):
    YEAR: Optional[Union[int, str]]
    MONTH1: Optional[Union[int, str]]
    MONTH2: Optional[Union[int, str]]



class ItemIngest(BaseModel):
    filename: str

class ItemRerun(BaseModel):
    filename: str

@app.post("/ingest", status_code=status.HTTP_200_OK)
async def ingest_data(body: ItemIngest):
    try:
        Truncate_raw_ProductSalesAmount()
        Load_excel_as_dataframe(file_name=body.filename)
        Load_dataframe_to_raw()
        Transform_and_Load()
        message = f"The `ProductSalesAmount Ingestion {body.filename}` pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200, "Message": "Ingestion Success."}
    except :
        message = f"The `ProductSalesAmount Ingestion {body.filename}` pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200, "Message": "Ingestion Failed."}
    

@app.post("/rerun", status_code=status.HTTP_200_OK)
async def ingest_data(body: ItemIngest):
    try:
        Truncate_raw_ProductSalesAmount()
        Load_excel_as_dataframe(file_name=body.filename)
        Load_dataframe_to_raw()
        delete_newer_data()
        Transform_and_Load()
        message = f"The `ProductSalesAmount RERUN {body.filename}` pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200, "Message": "RERUN Success."}
    except :
        message = f"The `ProductSalesAmount RERUN {body.filename}` pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200, "Message": "RERUN Failed."}



@app.post("/export", status_code=status.HTTP_200_OK)
async def export_data(date: DateQuery):
    try:
        export_data(date.YEAR, date.MONTH1, date.MONTH2)
        return {"status": 200, "message": "export data success"}
    except:
        return {"status": 400, "message": "Export data erorr"}
    

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}