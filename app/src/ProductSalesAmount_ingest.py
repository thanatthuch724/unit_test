
import os
import psycopg2
import pandas
from dotenv import load_dotenv

"""
 ------- PROCESS INGEST DATA OF `ProductSalesAmountByMonth`
"""

load_dotenv()

datafile ="./src/temp/tempfile.csv"

RAW_TABLE : str = "raw.ProductSalesAmountByMonth";
CRT_TABLE : str = "crt.ProductSalesAmountByMonth";

DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
SR_PATH : str = os.getenv("SR_PATH")

SQL_LOGIC : str = f"""
INSERT INTO {CRT_TABLE}        
SELECT A.yearMonth, A.ProductID, A.ProductName, A.salesAmount,
CASE WHEN A.yearMonth > B.yearMonth THEN ((A.salesAmount / B.salesAmount)-1) *100 ELSE NULL END percentage_change
FROM {RAW_TABLE} A
LEFT JOIN (
	SELECT t1.*
	FROM {CRT_TABLE} t1
	JOIN (
		SELECT ProductID, MAX(DATE(yearMonth || '-01')) AS max_start_date
		FROM {CRT_TABLE}
		GROUP BY ProductID 
	) t2
	ON t1.ProductID = t2.ProductID
	AND DATE(t1.yearMonth || '-01') = t2.max_start_date) B
ON A.ProductID = B.ProductID
WHERE NOT EXISTS (
	SELECT 1
	FROM {CRT_TABLE} C
	WHERE C.ProductID = A.ProductID
	AND C.yearMonth = A.yearMonth
);
"""


db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};



# PROCESS INGEST DATA OF `ProductSalesAmountByMonth`

"""
----------------------------------------------------------
Load data of new month > RAW_TABLE > Transform > CRT_TABLE
----------------------------------------------------------
""" 

# Load data from `xlsx` as `DataFrame`
def Load_excel_as_dataframe(file_name: str) -> pandas.DataFrame:
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    file_directory = f"{SR_PATH}/{file_name}"
    try:
        dataFrame = pandas.read_excel(file_directory)
        dataFrame = dataFrame[["yearMonth", "ProductID", "ProductName", "salesAmount"]]
        dataFrame.to_csv("./src/temp/tempfile.csv", index=False)
        return {"status": "success"}
    except :
        return {"status": "faill"} 

# Truncate data from `RAW_TABLE`
def Truncate_raw_ProductSalesAmount(raw_table: str = RAW_TABLE):
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    SQL = f"TRUNCATE {raw_table};";
    curosr = Connection.cursor()
    try:
        curosr.execute(SQL)
        Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}


# Load `DataFrame` into `RAW_TABLE`
def Load_dataframe_to_raw(raw_table: str=RAW_TABLE) -> None:
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    try:
        with open(datafile, "r") as data_file:
            cursor.copy_expert(f"COPY {raw_table} FROM stdin WITH CSV HEADER", data_file)
            Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}
    

# Transform and Load data logic to `CRT ZONE`
def Transform_and_Load(sql_logic: str=SQL_LOGIC):
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    try:
        cursor.execute(sql_logic)
        Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}


# file_directory = "/Users/spy/Documents/working_space/jenkins/unit_test/src/ProductSalesAmount/1996Aug.xlsx"
    
# Truncate_raw_ProductSalesAmount()
# Load_excel_as_dataframe(file_directory)
# Load_dataframe_to_raw()
# Transform_and_Load()




    




