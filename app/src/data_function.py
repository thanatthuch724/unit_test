import sqlite3
import pandas as pd

DATABASE : str = "./src/medcury-de.db"
TABLE  : str = "ProductSalesAmountByMonth"
EXPORT_PATH : str = "./src/ProductSalesAmount"


def convert_month(month):
    month_dict = {
        '01': 'Jan',
        '02': 'Feb',
        '03': 'Mar',
        '04': 'Apr',
        '05': 'May',
        '06': 'Jun',
        '07': 'Jul',
        '08': 'Aug',
        '09': 'Sep',
        '10': 'Oct',
        '11': 'Nov',
        '12': 'Dec'
    }
    year, month_number = month.split('-')
    return f"{month_dict[month_number]} {year}"


def export_data_as_excel(year, month1=None, month2=None):
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    file_name : str = ""
    if (month1 is None) and (month2 is None):
        sql_query = f"SELECT * FROM {TABLE} WHERE SUBSTR(yearMonth, 1,4)=='{year}';"
        file_name = f"{year}"
        excel_writer = pd.ExcelWriter(f'{EXPORT_PATH}/{file_name}.xlsx', engine='xlsxwriter')
        df = pd.read_sql(sql_query, con)
        # df['yearMonth'] = df['yearMonth'].apply(convert_month)
        for yearmonth, group in df.groupby('yearMonth'):
            yearmonth = convert_month(yearmonth)
            yearmonth=yearmonth.split(" ")[0]
            group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
        excel_writer._save()
    
    elif (month1 is not None) and (month2 is None):
        month1_zfil = str(month1).zfill(2)
        sql_query = f"SELECT * FROM {TABLE} WHERE yearMonth='{year}-{month1_zfil}'"
        df = pd.read_sql(sql_query, con)
        # df['yearMonth'] = df['yearMonth'].apply(convert_month)
        month1_name = f"{year}-{str(month1).zfill(2)}"
        month1_name = convert_month(month1_name)
        file_name = f"{year}"+str(month1_name).zfill(2).split(" ")[0]
        excel_writer = pd.ExcelWriter(f'{EXPORT_PATH}/{file_name}.xlsx', engine='xlsxwriter')
        for yearmonth, group in df.groupby('yearMonth'):
            yearmonth = convert_month(yearmonth)
            yearmonth=yearmonth.split(" ")[0]
            group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
        excel_writer._save()

        #######
    elif (month1 is None) and (month2 is not None):
        month2_zfil = str(month2).zfill(2)
        sql_query = f"SELECT * FROM {TABLE} WHERE yearMonth='{year}-{month2_zfil}'"
        df = pd.read_sql(sql_query, con)
        # df['yearMonth'] = df['yearMonth'].apply(convert_month)
        month2_name = f"{year}-{str(month2).zfill(2)}"
        month2_name = convert_month(month2_name)
        file_name = f"{year}"+str(month2_name).zfill(2).split(" ")[0]

        excel_writer = pd.ExcelWriter(f'{EXPORT_PATH}/{file_name}.xlsx', engine='xlsxwriter')
        for yearmonth, group in df.groupby('yearMonth'):
            yearmonth = convert_month(yearmonth)
            yearmonth=yearmonth.split(" ")[0]
            group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
        excel_writer._save()
        #######
    else:
        month1 = str(month1).zfill(2)
        month2 = str(month2).zfill(2)
        sql_query = f"SELECT * FROM {TABLE} WHERE yearMonth BETWEEN '{year}-{month1}' AND '{year}-{month2}';"
        df = pd.read_sql(sql_query, con)

        month1_name = f"{year}-{month1}"
        month1_name = convert_month(month1_name).split(" ")[0]
        month2_name = f"{year}-{month2}"
        month2_name = convert_month(month2_name).split(" ")[0]
        file_name = f"{year}{month1_name}-{month2_name}"
        excel_writer = pd.ExcelWriter(f'{EXPORT_PATH}/{file_name}.xlsx', engine='xlsxwriter')
        for yearmonth, group in df.groupby('yearMonth'):
            yearmonth = convert_month(yearmonth)
            yearmonth=yearmonth.split(" ")[0]
            group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
        excel_writer._save()
    con.close()
    return type(df), file_name

# export_data_as_excel(1996)